<?php
//
include '../Includes/Conexion.php';
include './vistaLateral.php';

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Personalizar un input file - Forma 1</title>
        <link rel="stylesheet" href="../assets/Styles/Vista_doc.css">
    </head>
    
    <body>
          <form  class="form"  method="POST" enctype="multipart/form-data" action="guardar.php">

              <div class="input-file input-file--reverse">
                <p> HOJA DE VIDA:</p>
              <label for="" class="input-file__field"  name ="record-academico"></label>
                <input type="file" id="file"  accept="application/pdf" class="input-file__input" name ="hoja">
                <label for="file" class="input-file__btn">Seleccionar archivo</label>
           </div>
            <div class="input-file input-file--reverse">
                <p> RECORD ACADEMICO:</p>
              <label for="" class="input-file__field"  name ="record-academico"></label>
                <input type="file" id="file1"  accept="application/pdf" class="input-file__input" name ="record">
                <label for="file1" class="input-file__btn">Seleccionar archivo</label>
           </div>
            <div class="input-file input-file--reverse">
                <p>CERTIFICADO DE VINCULACION:</p>
                <label for="" class="input-file__field"  name ="certificado-vinculacion"></label>
                <input type="file" id="file2"  accept="application/pdf" class="input-file__input" name ="vinculaciones">
                <label for="file2" class="input-file__btn">Seleccionar archivo</label>
            </div>
            <div class="input-file input-file--reverse">
                <p>CERTIFICADO DE PRACTICAS:</p>
                <label for="" class="input-file__field" name ="certificado-practicas"></label>
                <input type="file" id="file3" accept="application/pdf" class="input-file__input" name ="practicas" >
                <label for="file3" class="input-file__btn">Seleccionar archivo</label>
            </div>
            <div class="input-file input-file--reverse">
                <p>CERTIFICADO DE SIAU:</p>
                <label for="" class="input-file__field" name ="certificado-siau"></label>
                <input type="file" id="file4" accept="application/pdf" class="input-file__input" name ="siau">
                <label for="file4" class="input-file__btn">Seleccionar archivo</label>
            </div>
            <div class="input-file input-file--reverse">
                <p>CERTIFICADO DE INGLES:</p>
                <label for="" class="input-file__field" name ="certificado-ingles"></label>
                <input type="file" id="file5" accept="application/pdf" class="input-file__input" name ="ingles">
                <label for="file5" class="input-file__btn">Seleccionar archivo</label>
            </div>
            
             <input type="submit" value="enviar">
        </form>
        <script  src ="../js/jquery-3.3.1.min.js " ></script>
        <script  src =" ../js/scripts.js " ></script>
        
        
        
    </body>
</html>